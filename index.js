const fs = require("fs");
const {parse} = require("csv-parse");
const {minify} = require('html-minifier-terser');

if (fs.existsSync('./signatures')) {
    fs.rmSync('./signatures', {recursive: true})
}
fs.mkdirSync('./signatures')


let html = ``
const minifyOptions = {
    removeTagWhitespace: true,
    noNewlinesBeforeTagClose: true,
    removeCommentsFromCDATA: true,
    collapseWhitespace: true,
    // collapseBooleanAttributes: true,
    removeAttributeQuotes: true,
    // removeRedundantAttributes: true,
    // useShortDoctype: true,
    // removeEmptyAttributes: true,
    // removeOptionalTags: true,
    // removeEmptyElements: true,
    minifyCSS: true
}

const readAndCreateSignatures = () => {
    fs.createReadStream("data.csv")
        .pipe(parse({delimiter: ",", from_line: 2}))
        .on("data", async (row) => {
            let template = html.toString()

            if (row[5].trim() === '') {
                template = template
                    .replaceAll(
                        template
                            .substring(
                                template.indexOf("<!--{if telegram-->"),
                                template.indexOf("<!--end if telegram}-->") + '<!--end if telegram}-->'.length),
                        "")
            }

            if (row[4].trim() === '') {
                template = template
                    .replaceAll(
                        template
                            .substring(
                                template.indexOf("<!--{if tel-->"),
                                template.indexOf("<!--end if tel}-->") + '<!--end if tel}-->'.length),
                        "")
            }

            const modificatedHtml = await minify(
                template
                    .replace('{{img}}', row[7])
                    .replace('{{name}}', row[1] || row[0])
                    .replace('{{job}}', row[2])
                    .replaceAll('{{tel}}', row[4])
                    .replaceAll('{{mail}}', row[3])
                    .replaceAll('{{telegram}}', row[5]),

                minifyOptions
            )

            fs.writeFileSync(`./signatures/${row[1] || row[0]}.html`, modificatedHtml)
        })
        .on("end", function () {

        })
        .on("error", function (error) {
            console.log(error.message);
        });
}
fs.createReadStream('template/index.html')
    .on('data', (chunk) => {
        html += chunk
    })
    .on("end", () => {
        readAndCreateSignatures()
    })
    .on("error", function (error) {
        console.log(error.message);
    });


